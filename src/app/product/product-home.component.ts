import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from './product';
import { Vendor } from '../vendor/vendor';
import { MatTableDataSource } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { RestfulService } from '../restful.service';
import { BASEURL, PRODAPIURL } from '../constants';
import { MatPaginator, PageEvent } from '@angular/material';

@Component({
  selector: 'app-product-home',
  templateUrl: './product-home.component.html',
  styleUrls: ['./product-home.component.scss']
})
export class ProductHomeComponent implements OnInit {
  products: Product[];
  vendors: Array<Vendor>;
  selectedProduct: Product;

  //from exercise-client
  hideEditForm: boolean;
  msg: string;
  todo: string;
  url: string;
  emptyVendor: Vendor;
  displayedColumns: string[] = ['id', 'name', 'vendorid'];
  dataSource: MatTableDataSource<Product>;
  @ViewChild(MatSort, null) sort: MatSort;

  pageSize = 5;
  totalElements: number;
  currentPage: number;
  // get reference to paginator
  @ViewChild(MatPaginator, null) paginator: MatPaginator;


  constructor(private restService: RestfulService) {
    this.hideEditForm = true;
    this.url = BASEURL + 'products';

    this.emptyVendor = {
      id: null,
      name: '',
      address1: '',
      city: '',
      province: '',
      postalcode: '',
      phone: '',
      type: '',
      email: ''
    };
  }

  ngOnInit() {
    this.msg = 'loading vendors from server...';
    this.restService.load(BASEURL + 'vendors').subscribe(
      empPayload => {
        this.vendors = empPayload._embedded.vendors;
        this.msg = 'vendors loaded';
      },
      err => {
        this.msg += `Error occurred - vendors not loaded - ${err.status} - ${err.statusText}`;
      }
    );
    this.currentPage = 0;
    this.getPagedProducts();
  }

  select(product: Product) {
    this.todo = 'update';
    this.selectedProduct = product;
    this.msg = `Product ${product.name} selected`;
    this.hideEditForm = !this.hideEditForm;
  } // select
  /**
   * cancelled - event handler for cancel button
   */
  cancel(msg?: string) {
    this.restService.load(this.url).subscribe(
      payload => {
        this.getPagedProducts();
        this.products = payload._embedded.products;
        this.msg = 'Operation Cancelled!';
        this.dataSource.data = this.products;
        this.dataSource.sort = this.sort;
      },
      err => {
        this.msg += `Error occurred - products not loaded - ${err.status} - ${err.statusText}`;
      }
    );
    this.hideEditForm = !this.hideEditForm;
  } // cancel
  /**
   * update - send changed update to service update local array
   */
  update(product: Product) {
    this.msg = 'Updating...';
    console.log('updating ' + PRODAPIURL, product);
    this.restService.update(PRODAPIURL, product).subscribe(
      payload => {
        if (payload.id) {
          this.getPagedProducts();
          // update local array using ? operator with data returned from the server
          this.products = this.products.map(p =>
            p.id === product.id ? (Object as any).assign({}, p, payload) : p
          );
          this.msg = `Product ${product.id} updated!`;
          this.dataSource.data = this.products;
          this.dataSource.sort = this.sort;
        } else {
          this.msg = 'Product not updated! - Server problem';
        }
      },
      err => {
        this.msg = `Error - product not updated - ${err.status} - ${err.statusText}`;
      }
    );
    this.hideEditForm = !this.hideEditForm;
  } // update
  /**
   * save - determine whether we're doing and add or an update
   */
  save(product: Product) {
    //fixed way1 by adding  p.id === 
    this.products.find(p => p.id === product.id) ? this.update(product) : this.add(product);

    // { debug code   
    //   console.log("xxx =>" + this.products.find(p => p.id === "product.id"));
    //   const array1 = [5, 12, 8, 130, 44];
    //   const found1 = array1.find(element => element > 10);
    //   console.log("found1 " + found1);
    // }

    //way2
    // var found = false;
    // for (var i = 0; i < this.products.length; i++) {
    //   if (this.products[i].id === product.id) {
    //     found = true;
    //     break;
    //   }
    // }
    // if (found)//product.id)) 
    // {
    //   this.update(product);
    //   console.log("update fuck" + product.id);
    // }
    // else { this.add(product); console.log("add " + product.id); }
  } // save
  /**
   * add - send product to service, receive newid back
   */
  add(product: Product) {
    this.msg = 'Adding...';
    console.log('Adding');
    this.restService.add(PRODAPIURL, product).subscribe(
      payload => {
        if (payload.id !== '') {
          this.getPagedProducts();
          // server returns new id
          this.products = [...this.products, payload]; // add product to current array using spread
          this.msg = `Product ${product.id} added!`;
          this.dataSource.data = this.products;
          this.dataSource.sort = this.sort;
        } else {
          this.msg = 'Product not added! - server error';
        }
      },
      err => {
        this.msg = `Error - product not added - ${err.status} - ${err.statusText}`;
      }
    );
    this.hideEditForm = !this.hideEditForm;
  } // add
  /**
   * newProduct - create new product instance
   */
  newProduct() {
    (this.selectedProduct = {
      id: '',
      vendorid: 0,
      name: '',
      costprice: 0,
      msrp: 0,
      rop: 0,
      eoq: 0,
      qoh: 0,
      qoo: 0,
      qrcode: '',
      qrcodetxt: ''
    }),
      (this.msg = 'New product');
    this.hideEditForm = !this.hideEditForm;
  } // newProduct
  /**
   * delete - send product id to service for deletion and remove from local collection
   */
  delete(product: Product) {
    this.msg = 'Deleting...';
    console.log('deleting ->' + product.id);
    this.restService
      .load(`${this.url}/search/deleteOne?productid=${product.id}`)
      .subscribe(
        payload => {
          console.log('payload === ' + payload);
          if (payload === 1) {
            // server returns # rows deleted
            this.msg = `Product ${product.id} deleted!`;
            this.products = this.products.filter(exp => exp.id !== product.id);
            this.dataSource.data = this.products;
            this.dataSource.sort = this.sort;
          } else {
            this.msg = 'Product not deleted! - server error';
          }
        },
        err => {
          this.msg = `Error - vendors not deleted - ${err.status} - ${err.statusText}`;
        }
      );
    this.hideEditForm = !this.hideEditForm;
  } // delete

  changePage($pageEvent?: PageEvent) {
    this.currentPage = $pageEvent.pageIndex;
    this.getPagedProducts();
  } // changePage

  getPagedProducts() {
    this.msg = 'loading page of products...';
    this.restService.load(`${BASEURL}api/pagedproducts?&p=${this.currentPage}&s=${this.pageSize}`).subscribe(
      payload => {
        this.products = payload.content;
        this.dataSource = new MatTableDataSource(this.products);
        this.dataSource.sort = this.sort;
        this.msg = `page ${payload.number + 1} of products loaded`;
        if (this.totalElements !== payload.totalElements) {
          // reset paginator
          this.paginator.firstPage();
          this.totalElements = payload.totalElements;
        }
      },
      err => {
        this.msg += 'Error occurred - products not loaded - ' + err.status + ' - ' + err.statusText;
      });
  } // getPagedProducts

}
