import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root' //this means the service should be created by the 'ROOT APPLICATION INJECTOR'.   New With V6?
})
export class RestfulService {
  constructor(public http: HttpClient) {}

  /**
   * get
   */
  load(url) {
    return this.http.get<any>(url);
  }

  /**
   * update
   */
  update(url: string, entity: any) {
    return this.http.put<any>(url, entity);
  }

  add(url: string, entity: any) {
    return this.http.post<any>(url, entity);
  }

  // notice delete is a get
  delete(url) {
    return this.http.get<any>(url);
  }
} //restful service
