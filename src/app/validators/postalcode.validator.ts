import {AbstractControl} from '@angular/forms';

export function ValidatePostalcode(control: AbstractControl) {
  const POSTAL_CODE_REGEXP = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
  return !POSTAL_CODE_REGEXP.test(control.value) ? {invalidPostalcode: true} : null;
} // ValidatePhone
