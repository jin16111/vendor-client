import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoGeneratorComponent } from './po-generator.component';

describe('PoGeneratorComponent', () => {
  let component: PoGeneratorComponent;
  let fixture: ComponentFixture<PoGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
