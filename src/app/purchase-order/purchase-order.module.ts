import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoGeneratorComponent } from './po-generator/po-generator.component';
import {
  MatToolbarModule,
  MatCardModule,
  MatSelectModule,
  MatButtonModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PoGeneratorComponent],
  imports: [
    CommonModule,
    MatSelectModule,
    MatToolbarModule,
    MatCardModule,
    ReactiveFormsModule,
    MatButtonModule
  ]
})
export class PurchaseOrderModule {}
