import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Vendor } from './vendor';

@Component({
  selector: 'app-vendor-list',
  template: `
    <mat-list-item
      *ngFor="let vendor of vendors"
      (click)="selected.emit(vendor)"
      layout="row"
      class="pad-xs mat-title"
    >
      {{ vendor.id }} - {{ vendor.name }}
    </mat-list-item>
  `,
  styleUrls: ['./vendor-list.component.scss']
})
export class VendorListComponent implements OnInit {
  @Input() vendors: Vendor[];
  @Output() selected = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
