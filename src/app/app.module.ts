import { PurchaseOrderModule } from './purchase-order/purchase-order.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// tslint:disable-next-line:max-line-length
import {
  MatButtonModule,
  MatTooltipModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatSelectModule,
  MatTableModule,
  MatExpansionModule,
  MatSortModule,
  MatPaginatorModule
} from '@angular/material';

import { VendorHomeComponent } from './vendor/vendor-home.component';
import { VendorListComponent } from './vendor/vendor-list.component';
import { VendorDetailComponent } from './vendor/vendor-detail/vendor-detail.component';
import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ProductHomeComponent } from './product/product-home.component';
import { ProductDetailComponent } from './product/product-detail.component';
import { PoGeneratorComponent } from './purchase-order/po-generator/po-generator.component';

@NgModule({
  declarations: [
    VendorHomeComponent,
    VendorListComponent,
    VendorDetailComponent,
    HomeComponent,
    AppComponent,
    ProductHomeComponent,
    ProductDetailComponent // after you generate the vendorlistcomponent, it is declared here
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatToolbarModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatExpansionModule,
    MatSortModule,
    PurchaseOrderModule,
    MatPaginatorModule
  ],
  bootstrap: [AppComponent] // what is that bootstrap?
})
export class AppModule { }
