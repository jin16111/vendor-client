import { PurchaseOrderLineItem } from './purchase-order-line-item';

export interface PurchaseOrder {
  vendorid: number;
  amount: number;
  podate: Date;
  items: PurchaseOrderLineItem[];
}
