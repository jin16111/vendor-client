import { RestfulService } from './../../restful.service';
import { Vendor } from './../../vendor/vendor';
import { Product } from './../../product/product';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import { BASEURL, PDFURL } from 'src/app/constants';
import { PurchaseOrder } from '../purchase-order';
import { PurchaseOrderLineItem } from '../purchase-order-line-item';

@Component({
  selector: 'app-po-generator',
  templateUrl: './po-generator.component.html',
  styleUrls: ['./po-generator.component.scss']
})
export class PoGeneratorComponent implements OnInit {
  //form
  generatorForm: FormGroup;
  productid: FormControl;
  vendorid: FormControl;
  qty: FormControl;
  purchaseOrder: PurchaseOrder;
  //component
  vendors: Array<Vendor>;
  products: Array<Product>;
  selectedVendor: Vendor;
  selectedProduct: Product;
  selectedQty: number;

  pickedVendor = false;
  pickedProduct = false;
  pickedQty = false;
  generated: boolean;

  sub: number;
  tax: number;
  total: number;

  ponum: number;

  msg: string;

  constructor(
    private builder: FormBuilder,
    private restService: RestfulService
  ) { }

  ngOnInit() {
    this.msg = '';
    this.vendorid = new FormControl(
      '',
      Validators.compose([Validators.required])
    );
    this.productid = new FormControl(
      '',
      Validators.compose([Validators.required])
    );
    this.qty = new FormControl(-1, Validators.compose([Validators.required]));
    this.generatorForm = this.builder.group({
      productid: this.productid,
      vendorid: this.vendorid,
      qty: this.qty
    });

    this.msg = 'loading vendors from server...';
    this.restService.load(BASEURL + 'vendors').subscribe(
      payload => {
        this.vendors = payload._embedded.vendors;
        this.msg = 'vendors loaded';
      },
      err => {
        this.msg += `Error occurred - vendors not loaded - ${err.status} - ${err.statusText}`;
      }
    );

    this.purchaseOrder = {
      vendorid: 0,
      amount: 0,
      podate: null,
      items: null
    };

    this.onPickVendor();
  } // end ogOnInit

  /**
   * onPickVendor - subscribe to the select change event then load specific
   * vendor products for subsequent selection
   */
  onPickVendor(): void {
    //GET VENDOR FROM FORM #1
    this.generatorForm.get('vendorid').valueChanges.subscribe(val => {
      this.purchaseOrder.items = null;
      //set vendorid
      this.purchaseOrder.vendorid = this.generatorForm.value.vendorid;

      this.selectedVendor = val;

      this.msg = 'choose product of vendor';
      //load products
      this.loadVendorProducts();

      //display settings
      this.pickedVendor = true;
      this.pickedProduct = false;
      this.pickedQty = false;
      this.generated = false;
    });
  } //end onPickVendor
  loadVendorProducts() {
    this.restService
      .load(
        BASEURL +
        'products/search/getByVendorid?vendorid=' +
        this.selectedVendor.id
      )
      .subscribe(
        payload => {
          this.products = payload._embedded.products;
          this.msg = 'Choose product for vendor';
        },
        err => {
          this.msg = `Error occurred - products not loaded - ${err.status} - ${err.statusText}`;
        }
      );
  }

  onPickProduct(): void {
    //show qty
    this.pickedProduct = true;
    //reset QTY
    this.generatorForm.get('qty').setValue(-1);
    //GET PRODUCT FROM 2-WAY BINDING #2 [(value)]="selectedProduct"
    this.msg = `${this.selectedProduct.eoq} ${this.selectedProduct.name}(s) Added`;
    this.onPickQty();
  }

  onPickQty(): void {
    //GET QTY FROM FORM #1.1
    let qty = this.generatorForm.value.qty; // this is a number

    //get eoq value
    if (qty < 0) {
      qty = this.selectedProduct.eoq;
    }

    //remove items
    if (qty === 0) {
      this.msg = `all ${this.selectedProduct.name}s removed`;
      //see if the list is empty
    } else {
      this.msg = `${qty} ${this.selectedProduct.name}(s) Added`;
    }
    //show add order button
    this.pickedQty = true;

    this.addItem(qty);
  }

  addItem(q): void {
    //make new pol
    let pol: PurchaseOrderLineItem = {
      id: 0,
      poid: 0,
      productid: this.selectedProduct.id,
      qty: q,
      price: this.selectedProduct.costprice
    };

    if (!this.purchaseOrder.items) {
      this.purchaseOrder.items = [];
    }
    if (pol.qty === 0) {
      this.purchaseOrder.items = this.purchaseOrder.items.filter(
        p => p.productid !== pol.productid
      );
    } else if (
      this.purchaseOrder.items.find(it => it.productid === pol.productid)
    ) {
      this.purchaseOrder.items.find(it => it.productid === pol.productid).qty =
        pol.qty;
      this.purchaseOrder.items.find(
        it => it.productid === pol.productid
      ).price = pol.price;
    } else {
      // add entry
      this.purchaseOrder.items.push(pol);
    }
    this.getSummary();
  }

  getSummary() {
    this.sub = 0;
    this.purchaseOrder.items.map(item => (this.sub += item.price * item.qty));
    this.tax = (this.sub * 13) / 100;
    this.total = this.sub + this.tax;
  }

  generatePOrder() {
    this.purchaseOrder.amount = this.total;
    this.purchaseOrder.podate = new Date();
    this.purchaseOrder.vendorid = this.selectedVendor.id;

    this.restService.add(BASEURL + 'api/pos', this.purchaseOrder).subscribe(
      payload => {
        this.generated = true;
        this.msg = `PO ${payload} added`;
        this.ponum = payload;
      },
      err => {
        this.msg = `Error occurred - purchase order not added - ${err.status} - ${err.statusText}`;
      }
    );
  }
  /**
* viewPdf - determine report number and pass to server
* for PDF generation in a new window
*/
  viewPdf() {
    window.open(PDFURL + this.ponum, '');
  } // viewPdf
}
